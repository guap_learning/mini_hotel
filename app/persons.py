from lxml import html
import requests


def get_pers():
    pp =  requests.get('https://randus.org/api.php')
    return pp.json()


def get_person():
    page = requests.get('http://cadic.name/whoisgen')
    tree = html.fromstring(page.content)
    person = tree.xpath('//strong/text()')
    return person


def gen_person():
    pp = get_pers()
    p = get_person()
    date = [p[4].split('.')[2],p[4].split('.')[1],p[4].split('.')[0]]
    client = {"first": pp['fname'], "last": pp['lname'], "midle": pp['patronymic'], "passport": p[2].replace('Москвы', pp['city']),"login": pp['login'], "password": pp['login'],  "registration": p[3].replace('Москва', pp['city']), "birthday": date,
              "gender": pp['gender']}
    return client


def rand_date(t):
    from random import randint
    if t == 'yy':
        r = randint(2017, 2019)
    elif t == 'mm':
        r = randint(1, 12)
    elif t == 'dd':
        r = randint(1, 28)
    return int(r)
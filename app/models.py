from flask_login import UserMixin
from sqlalchemy import CheckConstraint
from random import randint
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, login_manager
from sqlalchemy import *


class Employee(UserMixin, db.Model):
    """
    Create an Employee table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'employees'

    id = db.Column(db.Integer, db.ForeignKey('maids.id'), primary_key=True, autoincrement=True)
    email = db.Column(db.String(60), index=True, unique=True)
    username = db.Column(db.String(60), index=True, unique=True)
    first_name = db.Column(db.String(60), index=True)
    last_name = db.Column(db.String(60), index=True)
    middle_name = db.Column(db.String(60), index=True)
    passport_data = db.Column(db.Text, nullable=False, unique=True, default=first_name+last_name+email+str(id))
    password_hash = db.Column(db.String(128))
    department_id = db.Column(db.Integer, db.ForeignKey('departments.id'))
    date_of_employment = db.Column(db.Date, nullable=False, default=datetime(2000,1,1,10, 10, 10).date())
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    is_admin = db.Column(db.Boolean, default=False)
    def pasport(self):
        a = randint()
        b =  str(a)+"5345345345345"
        return b
    @property
    def password(self):
        """
        Prevent pasword from being accessed
        """
        raise AttributeError('password is not a readable attribute.')

    @password.setter
    def password(self, password):
        """
        Set password to a hashed password
        """
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<Employee: {}>'.format(self.username)


# Set up user_loader
@login_manager.user_loader
def load_user(user_id):
    return Employee.query.get(int(user_id))


class Department(db.Model):
    """
    Create a Department table
    """

    __tablename__ = 'departments'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(60), unique=True)
    description = db.Column(db.String(200))
    employees = db.relationship('Employee', backref='department', lazy='dynamic')

    def __repr__(self):
        return '<Department: {}>'.format(self.name)


class Role(db.Model):
    """
    Create a Role table
    """
    __tablename__ = 'roles'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(60), unique=True)
    description = db.Column(db.String(200))
    employees = db.relationship('Employee', backref='role', lazy='dynamic')

    def __repr__(self):
        return '<Role: {}>'.format(self.name)


class RoomsClass(db.Model):
    """
    Create a RoomsClass table
    """
    __tablename__ = 'rooms_class'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    class_name = db.Column(db.Text, nullable=False)
    tv = db.Column(db.Boolean, nullable=False)
    internet = db.Column(db.Boolean, nullable=False)
    air_conditioning = db.Column(db.Boolean, nullable=False)
    kitchen = db.Column(db.Boolean, nullable=False)
    fitness = db.Column(db.Boolean, nullable=False)
    dinner = db.Column(db.Boolean, nullable=False)
    breakfast = db.Column(db.Boolean, nullable=False)
    rooms = db.relationship("Rooms", back_populates="rooms_class")

    #rooms = db.relationship("Rooms", back_populates="rooms_class")
    #room = db.relationship("Rooms", back_populates="rooms_class")
    def __repr__(self):
        return '<RoomsClass: {}>'.format(self.name)

class Maids(db.Model):
    """
    Maids Class
    """
    __tablename__ = 'maids'

    id = db.Column(db.Integer, db.ForeignKey('maids.id'), primary_key=True, autoincrement=True)
    employees = db.relationship('Employee', backref='maid', lazy='joined')
    rooms = db.relationship('Rooms', backref='maid', lazy=True)

    @property
    def __repr__(self):
        return '<Maids: {}>'.format(self.name)



class Rooms(db.Model):
    """
    Rooms Model
    """
    __tablename__ = 'rooms'
    id = db.Column(db.Integer, db.ForeignKey('maids.id'), primary_key=True, autoincrement=True)
    number = db.Column(db.Integer, nullable=False)
    bed = db.Column(db.Integer, nullable=False)
    price = db.Column(db.Integer, nullable=False)
    orders = db.relationship('Orders', backref='room', lazy=True)
    maids = db.relationship('Maids', backref='room', lazy=True)
    class_id = db.Column(Integer, ForeignKey('rooms_class.id'))
    rooms_class = db.relationship("RoomsClass", back_populates="rooms")
    #rooms_class = db.relationship("RoomsClass", back_populates="rooms")
    #rooms_class = db.Column(db.Integer, db.ForeignKey('rooms_class.id'), nullable=False)
    #rooms_class = db.relationship("RoomsClass", back_populates="room")
    orders = db.relationship('Orders', back_populates="room")
    def __repr__(self):
        return '<Rooms: {}>'.format(self.name)


class Clients(UserMixin, db.Model):
    """
    Create an Clients table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'clients'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(60), index=True, unique=True)
    username = db.Column(db.String(60), index=True, unique=True)
    first_name = db.Column(db.String(60), index=True)
    last_name = db.Column(db.String(60), index=True)
    middle_name = db.Column(db.String(60), index=True)
    passport = db.Column(db.Text, nullable=True, unique=True)
    registration = db.Column(db.Text, nullable=True)
    birthday = db.Column(db.Date, nullable=False)
    password_hash = db.Column(db.String(128))

    orders = db.relationship('Orders', back_populates="client")
    def pasport(self):
        a = randint()
        b =  str(a)+"5345345345345"
        return b
    @property
    def password(self):
        """
        Prevent pasword from being accessed
        """
        raise AttributeError('password is not a readable attribute.')

    @password.setter
    def password(self, password):
        """
        Set password to a hashed password
        """
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '{f} {n} {o}'.format(f=self.last_name, n=self.first_name, o=self.middle_name)


class Orders(db.Model):
    """
    Create a Orders table
    """
    __tablename__ = 'orders'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    client_id = db.Column(db.Integer, db.ForeignKey('clients.id'), nullable=False)
    room_id = db.Column(db.Integer, db.ForeignKey('rooms.id'), nullable=False)
    date_of_arrival = db.Column(db.Date, nullable=False )
    date_of_leaving = db.Column(db.Date, nullable=False )
    room_booked = db.Column(db.Boolean, nullable=False)
    order_paid = db.Column(db.Boolean, nullable=False)
    total_payment = db.Column(db.Integer, nullable=False)

    client = db.relationship("Clients", back_populates="orders")
    room = db.relationship("Rooms", back_populates="orders")
    CheckConstraint('date_of_arrival < date_of_leaving ', name='date_arrival_leaving')
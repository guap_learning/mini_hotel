from flask import abort, flash, redirect, render_template, url_for
from flask_login import current_user, login_required
from . import admin
from .forms import DepartmentForm,ClientForm, EmployeeAssignForm, OrdersForm, RoleForm, MaidsForm, RoomsClassForm, EmployeeForm
from .. import db
from ..models import Department, Employee, Role, Maids, RoomsClass, Clients, Orders


def check_admin():
    """
    Prevent non-admins from accessing the page
    """
    if not current_user.is_admin:
        abort(403)


# Employees Views

@admin.route('/employees')
@login_required
def list_employees():
    """
    List all employees
    """
    check_admin()
    employees = Employee.query.all()
    return render_template('admin/employees/employees.html', employees=employees, title='Сотрудники')

@admin.route('/employees/add', methods=['GET', 'POST'])
@login_required
def add_employee():
    """
    Add a role to the database
    """
    check_admin()
    add_employee = True
    form = EmployeeForm()
    if form.validate_on_submit():
        employee = Employee(
            email=form.email.data,
            username=form.username.data,
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            middle_name=form.middle_name.data,
            passport_data=form.passport_data.data,
            password=form.password.data,
            date_of_employment=form.date_of_employment.data,
            is_admin=form.is_admin.data
        )
        try:
            # add role to the database
            db.session.add(employee)
            db.session.commit()
            flash('Вы успешно добавили нового сотрудника.')
        except:
            # in case role name already exists
            flash('Ошибка: сотрудник уже существует.')
        # redirect to the roles page
        return redirect(url_for('admin.list_employees'))
    # load role template
    return render_template('admin/employees/employee.html', add_employee=add_employee, form=form, title='Добавить сотрудника')

@admin.route('/employees/assign_employee/<int:id>', methods=['GET', 'POST'])
@login_required
def assign_employee(id):
    """
    Assign a department and a role to an employee
    """
    check_admin()
    employee = Employee.query.get_or_404(id)
    # prevent admin from being assigned a department or role
    if employee.is_admin:
        abort(403)

    form = EmployeeAssignForm(obj=employee)
    if form.validate_on_submit():
        employee.department = form.department.data
        employee.role = form.role.data
        db.session.add(employee)
        db.session.commit()
        flash('Вы успешно назначили отдел и должность.')
        # redirect to the roles page
        return redirect(url_for('admin.list_employees'))
    return render_template('admin/employees/employee.html', employee=employee, form=form, title='Назначение Сотрудника')


# Department Views
@admin.route('/departments', methods=['GET', 'POST'])
@login_required
def list_departments():
    """
    List all departments
    """
    check_admin()
    departments = Department.query.all()
    return render_template('admin/departments/departments.html', departments=departments, title="Departments")


@admin.route('/departments/add', methods=['GET', 'POST'])
@login_required
def add_department():
    """
    Add a department to the database
    """
    check_admin()

    add_department = True

    form = DepartmentForm()
    if form.validate_on_submit():
        department = Department(name=form.name.data, description=form.description.data)
        try:
            # add department to the database
            db.session.add(department)
            db.session.commit()
            flash('You have successfully added a new department.')
        except:
            # in case department name already exists
            flash('Error: department name already exists.')
        # redirect to departments page
        return redirect(url_for('admin.list_departments'))
    # load department template
    return render_template('admin/departments/department.html', action="Add",
                           add_department=add_department, form=form,title="Add Department")


@admin.route('/departments/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_department(id):
    """
    Edit a department
    """
    check_admin()
    add_department = False
    department = Department.query.get_or_404(id)
    form = DepartmentForm(obj=department)
    if form.validate_on_submit():
        department.name = form.name.data
        department.description = form.description.data
        db.session.commit()
        flash('You have successfully edited the department.')
        # redirect to the departments page
        return redirect(url_for('admin.list_departments'))
    form.description.data = department.description
    form.name.data = department.name
    return render_template('admin/departments/department.html', action="Edit",
                           add_department=add_department, form=form, department=department, title="Edit Department")


@admin.route('/departments/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_department(id):
    """
    Delete a department from the database
    """
    check_admin()
    department = Department.query.get_or_404(id)
    db.session.delete(department)
    db.session.commit()
    flash('You have successfully deleted the department.')
    # redirect to the departments page
    return redirect(url_for('admin.list_departments'))
    return render_template(title="Delete Department")



# Roles_classes Views
@admin.route('/roles')
@login_required
def list_roles():
    check_admin()
    """
    List all roles
    """
    roles = Role.query.all()
    return render_template('admin/roles/roles.html', roles=roles, title='Roles')

@admin.route('/roles/add', methods=['GET', 'POST'])
@login_required
def add_role():
    """
    Add a role to the database
    """
    check_admin()
    add_role = True
    form = RoleForm()
    if form.validate_on_submit():
        role = Role(name=form.name.data,
                    description=form.description.data)
        try:
            # add role to the database
            db.session.add(role)
            db.session.commit()
            flash('Вы успешно добавили новую должность.')
        except:
            # in case role name already exists
            flash('Ошибка: имя должности уже существует.')
        # redirect to the roles page
        return redirect(url_for('admin.list_roles'))
    # load role template
    return render_template('admin/roles/role.html', add_role=add_role, form=form, title='Add Role')


@admin.route('/roles/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_role(id):
    """
    Edit a role
    """
    check_admin()
    add_role = False
    role = Role.query.get_or_404(id)
    form = RoleForm(obj=role)
    if form.validate_on_submit():
        role.name = form.name.data
        role.description = form.description.data
        db.session.add(role)
        db.session.commit()
        flash('You have successfully edited the role.')

        # redirect to the roles page
        return redirect(url_for('admin.list_roles'))
    form.description.data = role.description
    form.name.data = role.name
    return render_template('admin/roles/role.html', add_role=add_role,
                           form=form, title="Edit Role")


@admin.route('/roles/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_role(id):
    """
    Delete a role from the database
    """
    check_admin()
    role = Role.query.get_or_404(id)
    db.session.delete(role)
    db.session.commit()
    flash('You have successfully deleted the role.')
    # redirect to the roles page
    return redirect(url_for('admin.list_roles'))
    return render_template(title="Delete Role")


# Maids Views
@admin.route('/maids')
@login_required
def list_maids():
    check_admin()
    """
    List all maids
    """
    maids = Maids.query.all()
    return render_template('admin/maids/rooms_classes.html', maids=maids, title='Maids')


@admin.route('/maids/add', methods=['GET', 'POST'])
@login_required
def add_maid():
    """
    Add a role to the database
    """
    check_admin()
    add_maid = True
    form = MaidsForm()
    if form.validate_on_submit():
        maid = Maids(last_name=form.last_name.data,
                     first_name=form.first_name.data,
                     middle_name=form.middle_name.data,
                     passport_data=form.passport_data.data,
                     date_of_employment=form.date_of_employment.data)
        try:
            # add role to the database
            db.session.add(maid)
            db.session.commit()
            flash('You have successfully added a new maids.')
        except:
            # in case role name already exists
            flash('Error: maids name already exists.')
        # redirect to the roles page
        return redirect(url_for('admin.list_maids'))
    # load role template
    return render_template('admin/maids/rooms_class.html', add_maid=add_maid, form=form, title='Add Maid')


@admin.route('/maids/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_maids(id):
    """
    Edit a role
    """
    check_admin()
    add_maid = False
    maid = Maids.query.get_or_404(id)
    form = MaidsForm(obj=maid)
    if form.validate_on_submit():
        maid.last_name = form.last_name.data,
        maid.first_name = form.first_name.data,
        maid.middle_name = form.middle_name.data,
        maid.passport_data = form.passport_data.data,
        maid.date_of_employment = form.date_of_employment.data
        db.session.add(maid)
        db.session.commit()
        flash('You have successfully edited the maids.')
        # redirect to the roles page
        return redirect(url_for('admin.list_maids'))
    form.last_name = maid.last_name,
    form.first_name = maid.first_name,
    form.middle_name = maid.middle_name,
    form.passport_data = maid.passport_data,
    form.date_of_employment = maid.date_of_employment
    return render_template('admin/maids/rooms_class.html', add_maid=add_maid, form=form, title="Edit Maid")


@admin.route('/maids/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_maid(id):
    """
    Delete a maids from the database
    """
    check_admin()

    maid = Maids.query.get_or_404(id)
    db.session.delete(maid)
    db.session.commit()
    flash('You have successfully deleted the role.')
    # redirect to the roles page
    return redirect(url_for('admin.list_maids'))
    return render_template(title="Delete Maid")


# Rooms_classes Views
@admin.route('/rooms_classes')
@login_required
def list_rooms_classes():
    check_admin()
    """
    List all rooms_classes
    """
    rooms_classes = RoomsClass.query.all()
    return render_template('admin/rooms_classes/rooms_classes.html', rooms_classes=rooms_classes, title='Rooms Class')


@admin.route('/rooms_classes/add', methods=['GET', 'POST'])
@login_required
def add_rooms_class():
    """
    Add a role to the database
    """
    check_admin()
    add_rooms_class = True
    form = RoomsClassForm()
    if form.validate_on_submit():
        rooms_class = Maids(class_name=form.class_name.data,
                     tv=form.tv.data,
                     internet=form.internet.data,
                     air_conditioning=form.air_conditioning.data,
                     kitchen=form.kitchen.data,
                     fitness=form.fitness.data,
                     dinner=form.dinner.data,
                     breakfast=form.breakfast.data)
        try:
            # add role to the database
            db.session.add(rooms_class)
            db.session.commit()
            flash('You have successfully added a new rooms class.')
        except:
            # in case role name already exists
            flash('Error: rooms class name already exists.')
        # redirect to the roles page
        return redirect(url_for('admin.list_rooms_classes'))
    # load role template
    return render_template('admin/rooms_classes/rooms_class.html', add_maid=add_rooms_class, form=form, title='Add Rooms Class')


@admin.route('/rooms_classes/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_rooms_classes(id):
    """
    Edit a role
    """
    check_admin()
    add_rooms_class = False
    rooms_class = RoomsClass.query.get_or_404(id)
    form = MaidsForm(obj=rooms_class)
    if form.validate_on_submit():
        rooms_class.class_name = form.class_name.data,
        rooms_class.tv = form.tv.data,
        rooms_class.internet = form.internet.data,
        rooms_class.air_conditioning = form.air_conditioning.data,
        rooms_class.kitchen = form.kitchen.data,
        rooms_class.fitness = form.fitness.data,
        rooms_class.dinner = form.dinner.data,
        rooms_class.breakfast = form.breakfast.data
        db.session.add(rooms_class)
        db.session.commit()
        flash('You have successfully edited the rooms classes.')
        # redirect to the roles page
        return redirect(url_for('admin.list_rooms_classes'))
    form.class_name.data = rooms_class.class_name,
    form.tv.data = rooms_class.tv,
    form.internet.data = rooms_class.internet,
    form.air_conditioning.data = rooms_class.air_conditioning,
    form.kitchen.data = rooms_class.kitchen,
    form.fitness.data = rooms_class.fitness,
    form.dinner.data = rooms_class.dinner,
    form.breakfast.data = rooms_class.breakfast
    return render_template('admin/rooms_classes/rooms_class.html', add_rooms_class=add_rooms_class, form=form, title="Edit Room Class")


@admin.route('/rooms_classes/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_rooms_class(id):
    """
    Delete a maids from the database
    """
    check_admin()
    rooms_class = RoomsClass.query.get_or_404(id)
    db.session.delete(rooms_class)
    db.session.commit()
    flash('You have successfully deleted the role.')
    # redirect to the roles page
    return redirect(url_for('admin.list_rooms_class'))
    return render_template(title="Delete Room Class")


# Clients Views
@admin.route('/clients')
@login_required
def list_clients():
    check_admin()
    """
    List all clients
    """
    clients = Clients.query.all()
    return render_template('admin/clients/clients.html', clients=clients, title='Clients')

@admin.route('/clients/add', methods=['GET', 'POST'])
@login_required
def add_client():
    """
    Add a client to the database
    """
    check_admin()
    add_client = True
    form = ClientForm()
    if form.validate_on_submit():
        client = Clients(username=form.username.data,
                         first_name=form.first_name.data,
                         last_name=form.last_name.data,
                         middle_name=form.middle_name.data,
                         passport=form.passport.data,
                         registration=form.registration.data,
                         birthday=form.birthday.data,
                         email=form.email.data,
                         password=form.password.data)
        try:
            # add role to the database
            db.session.add(client)
            db.session.commit()
            flash('Вы успешно добавили нового клиента.')
        except:
            # in case role name already exists
            flash('Ошибка: имя клиента уже существует.')
        # redirect to the roles page
        return redirect(url_for('admin.list_clients'))
    # load role template
    return render_template('admin/clients/clients.html', add_client=add_client, form=form, title='Add Client')


@admin.route('/clients/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_client(id):
    """
    Edit a client
    """
    check_admin()
    add_client = False
    client = Clients.query.get_or_404(id)
    form = ClientForm(obj=client)
    if form.validate_on_submit():
        client.username = form.username.data,
        client.first_name = form.first_name.data,
        client.last_name = form.last_name.data,
        client.middle_name = form.middle_name.data,
        client.passport = form.passport.data,
        client.registration = form.registration.data,
        client.birthday = form.birthday.data,
        client.email = form.email.data,
        client.password = form.password.data
        db.session.add(client)
        db.session.commit()
        flash('You have successfully edited the role.')
        # redirect to the clients page
        return redirect(url_for('admin.list_clients'))
    return render_template('admin/clients/client.html', add_client=add_client, form=form, title="Edit Client")


@admin.route('/clients/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_client(id):
    """
    Delete a client from the database
    """
    check_admin()
    client = Clients.query.get_or_404(id)
    db.session.delete(client)
    db.session.commit()
    flash('You have successfully deleted the client.')
    # redirect to the roles page
    return redirect(url_for('admin.list_clients'))
    return render_template(title="Delete Client")


# Orders Views
@admin.route('/orders')
@login_required
def list_orders():
    check_admin()
    """
    List all orders
    """
    orders = Orders.query.all()
    return render_template('admin/orders/orders.html', orders=orders, title='Orders')


@admin.route('/orders/add', methods=['GET', 'POST'])
@login_required
def add_order():
    """
    Add a order to the database
    """
    check_admin()
    add_order = True
    form = OrdersForm()
    if form.validate_on_submit():
        order = Orders(client_id=form.client_id.data,
                         room_id=form.room_id.data,
                         date_of_arrival=form.date_of_arrival.data.strftime('%Y-%m-%d'),
                         date_of_leaving=form.date_of_leaving.data.strftime('%Y-%m-%d'),
                         room_booked=form.room_booked.data,
                         order_paid=form.order_paid.data,
                         total_payment=form.total_payment.data)
        try:
            # add role to the database
            db.session.add(order)
            db.session.commit()
            flash('Вы успешно добавили нового order.')
        except:
            # in case role name already exists
            flash('Ошибка: order уже существует.')
        # redirect to the roles page
        return redirect(url_for('admin.list_orders'))
    # load role template
    return render_template('admin/orders/order.html', add_order=add_order, form=form, title='Add Order')


@admin.route('/orders/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_order(id):
    """
    Edit a order
    """
    check_admin()
    add_order = False
    order = Orders.query.get_or_404(id)
    form = OrdersForm(obj=order)
    if form.validate_on_submit():
        order.client_id = form.client_id.data,
        order.room_id = form.room_id.data,
        order.date_of_arrival = form.date_of_arrival.data.strftime('%Y-%m-%d'),
        order.date_of_leaving = form.date_of_leaving.data.strftime('%Y-%m-%d'),
        order.room_booked = form.room_booked.data,
        order.order_paid = form.order_paid.data,
        order.total_payment = form.total_payment.data
        db.session.add(order)
        db.session.commit()
        flash('You have successfully edited the order.')
        # redirect to the clients page
        return redirect(url_for('admin.list_orders'))
    return render_template('admin/orders/order.html', add_order=add_order, form=form, title="Edit Order")


@admin.route('/orders/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_order(id):
    """
    Delete a order from the database
    """
    check_admin()
    order = Orders.query.get_or_404(id)
    db.session.delete(order)
    db.session.commit()
    flash('You have successfully deleted the order.')
    # redirect to the roles page
    return redirect(url_for('admin.list_orders'))
    return render_template(title="Delete Order")

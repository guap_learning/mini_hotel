from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, DateField, SelectField, BooleanField, IntegerField, TextField, PasswordField, ValidationError, validators
from wtforms.validators import DataRequired, Email, EqualTo
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.fields.html5 import DateField
from ..models import Department, Role, Maids, RoomsClass, Employee, Clients, Rooms
from datetime import date


class DepartmentForm(FlaskForm):
    """
    Form for admin to add or edit a department
    """
    name = StringField('Название', validators=[DataRequired()])
    description = StringField('Описание', validators=[DataRequired()])
    submit = SubmitField('Добавить')


class RoleForm(FlaskForm):
    """
    Form for admin to add or edit a role
    """
    name = StringField('Название', validators=[DataRequired()])
    description = StringField('Описание', validators=[DataRequired()])
    submit = SubmitField('Добавить')


class EmployeeForm(FlaskForm):
    """
    Form for admin to add employees
    """
    email = StringField('Email', validators=[DataRequired(), Email()])
    username = StringField('Имя пользователя', validators=[DataRequired()])
    first_name = StringField('Имя', validators=[DataRequired()])
    last_name = StringField('Фамилия', validators=[DataRequired()])
    middle_name = StringField('Отчество', validators=[DataRequired()])
    passport_data = TextField('Паспортные данные', validators=[DataRequired()])
    password = PasswordField('Пароль', validators=[DataRequired(), EqualTo('confirm_password')])
    confirm_password = PasswordField('Подтверждение пароля', validators=[DataRequired()])
    date_of_employment = DateField('Дата трудоустройства', validators=[DataRequired()])
    is_admin = BooleanField('Администратор')
    submit = SubmitField('Добавить')

    def validate_email(self, field):
        if Employee.query.filter_by(email=field.data).first():
            raise ValidationError('Электронная почта уже используется.')

    def validate_username(self, field):
        if Employee.query.filter_by(username=field.data).first():
            raise ValidationError('Имя пользователя уже используется.')


class EmployeeAssignForm(FlaskForm):
    """
    Form for admin to assign departments and roles to employees
    """
    department = QuerySelectField(query_factory=lambda: Department.query.all(), get_label="name")
    role = QuerySelectField(query_factory=lambda: Role.query.all(), get_label="name")
    submit = SubmitField('Назначить')


class MaidsForm(FlaskForm):
    """
    Form for admin
    """
    last_name = StringField('Last name', validators=[DataRequired()])
    first_name = StringField('First name', validators=[DataRequired()])
    middle_name = StringField('Middle name', validators=[DataRequired()])
    passport_data = StringField('Passport data', validators=[DataRequired()])
    date_of_employment = DateField('Date of employment', validators=[DataRequired()])
    submit = SubmitField('Submit')


class RoomsClassForm(FlaskForm):
    """
    Form for admin
    """
    class_name = StringField('Класс', validators=[DataRequired()])
    tv = BooleanField('TV', validators=[DataRequired()])
    internet = BooleanField('Internet', validators=[DataRequired()])
    air_conditioning = BooleanField('Кондиционер', validators=[DataRequired()])
    kitchen = BooleanField('Кухня', validators=[DataRequired()])
    fitness = BooleanField('Фитнесс', validators=[DataRequired()])
    dinner = BooleanField('Ужин', validators=[DataRequired()])
    breakfast = BooleanField('Завтрак', validators=[DataRequired()])
    submit = SubmitField('Добавить')

class RoomsForm(FlaskForm):
    """
    Form for admin
    """
    number = IntegerField('Номер комнаты', validators=[DataRequired()])
    class_id = QuerySelectField(query_factory=lambda: RoomsClass.query.all(), get_label="class_name")
    bed = IntegerField('Спальных мест', validators=[DataRequired()])
    price = IntegerField('Цена', validators=[DataRequired()])
    maid_id = QuerySelectField(query_factory=lambda: Maids.query.all(), get_label="first_name")
    submit = SubmitField('Добавить')


class ClientForm(FlaskForm):
    """
    Form for admin to add or edit a client
    """
    username = StringField('Имя пользователя', validators=[DataRequired()])
    first_name = StringField('Имя', validators=[DataRequired()])
    last_name = StringField('Фамилия', validators=[DataRequired()])
    middle_name = StringField('Отчество', validators=[DataRequired()])
    passport = TextField('Паспортные данные', validators=[DataRequired()])
    registration = TextField('Регистрация', validators=[DataRequired()])
    birthday = DateField('Описание', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired()])
    password = PasswordField('Пароль', validators=[DataRequired(), EqualTo('confirm_password')])
    confirm_password = PasswordField('Подтверждение пароля', validators=[DataRequired()])
    submit = SubmitField('Добавить')
    def validate_email(self, field):
        if Employee.query.filter_by(email=field.data).first():
            raise ValidationError('Электронная почта уже используется.')

    def validate_username(self, field):
        if Employee.query.filter_by(username=field.data).first():
            raise ValidationError('Имя пользователя уже используется.')

def get_projects():
    return Clients.query

class OrdersForm(FlaskForm):
    """
    Form for admin Orders
    """
    client_id = QuerySelectField(query_factory=get_projects)
    beds = SelectField(u'Спальных мест', choices=[(1, 1), (2, 2), (3, 3), (4, 4)])#q
    # uery_factory=lambda: Clients.query.all(), get_label="last_name")
    room_id = QuerySelectField( query_factory=lambda: Rooms.query, get_label="number")
    date_of_arrival =DateField('DatePicker', default=date.today, validators=[DataRequired()])#DateField('date_of_arrival', validators=[DataRequired()])
    date_of_leaving =DateField('DatePicker', validators=[DataRequired()])#DateField('date_of_leaving', validators=[DataRequired()])
    total_payment = IntegerField('Цена', validators=[DataRequired()])
    room_booked = BooleanField('room_booked', validators=[DataRequired()])
    order_paid = BooleanField('order_paid')
    submit = SubmitField('Добавить')

    def __init__(self, bed):
        super(OrdersForm, self).__init__()
        self.beds.choices = bed
    def validate_date_of_arrival(self):
        if self.date_of_arrival > self.date_of_leaving:
            raise ValidationError('Дата заезда не может быть позднее даты выезда')
from typing import List
from datetime import datetime, timedelta
from random import randint
from flask import Flask, abort, render_template
from flask_sqlalchemy import SQLAlchemy

# local imports
from config import app_config
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_bootstrap import Bootstrap



# db variable initialization
db = SQLAlchemy()
login_manager = LoginManager()


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config['development']) #config_name
    app.config.from_pyfile('config.py')
    Bootstrap(app)
    db.init_app(app)

    login_manager.init_app(app)
    login_manager.login_message = "Для доступа к данной странице необходимо авторизаваться."
    login_manager.login_view = "auth.login"
    migrate = Migrate(app, db, render_as_batch=True)

    from app import models

    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .home import home as home_blueprint
    app.register_blueprint(home_blueprint)

    #from app import RoomsClass
    with app.app_context():
        db.create_all()
        from app.models import RoomsClass, Rooms, Employee, Role, Department, Clients, Orders
        from .persons import gen_person, rand_date
        rooms_class = [
            RoomsClass(class_name='Стандарт', tv=True, internet=False, air_conditioning=False,kitchen=False, fitness=False, dinner=False, breakfast=True),
            RoomsClass(class_name='Комфорт', tv=True, internet=True, air_conditioning=True, kitchen=False, fitness=True, dinner=True, breakfast=True),
            RoomsClass(class_name='Люкс', tv=True, internet=True, air_conditioning=True, kitchen=False, fitness=True, dinner=False, breakfast=False),
            RoomsClass(class_name='Апартаменты', tv=True, internet=True, air_conditioning=True, kitchen=True, fitness=False, dinner=False, breakfast=False)
            ]
        for i in rooms_class:
            if db.session.query(RoomsClass.id).filter_by(class_name=i.class_name).scalar() is None:
                db.session.add(i)
                db.session.commit()
        roles = [
            Role(name="Старший администратор", description="Управляющий"),
            Role(name="Администратор", description="Регистрация клиентов"),
            Role(name="Старшая горничная", description="Старшая горничная"),
            Role(name="Горничная", description="Горничная"),
            Role(name="Инженер", description="Инженер")
        ]
        for i in roles:
            if db.session.query(Role.id).filter_by(name=i.name).scalar() is None:
                db.session.add(i)
                db.session.commit()
        departments = [
            Department(name="Служба приема и размещения",description="none"),
            Department(name="Администратор гостиницы", description="none"),
            Department(name="Служба горничных", description="none"),
            Department(name="Отдел бронирования", description="none"),
            Department(name="Управляющий отелем", description="none")
        ]
        for i in departments:
            if db.session.query(Department.id).filter_by(name=i.name).scalar() is None:
                db.session.add(i)
                db.session.commit()
        d1 = db.session.query(Department.id).filter(Department.name == 'Служба горничных').first()
        d2 = db.session.query(Department.id).filter(Department.name == 'Администратор гостиницы').first()
        d3 = db.session.query(Department.id).filter(Department.name == 'Служба приема и размещения').first()
        d4 = db.session.query(Department.id).filter(Department.name == 'Управляющий отелем').first()

        r1 = db.session.query(Role.id).filter(Role.name == 'Старшая горничная').first()
        r2 = db.session.query(Role.id).filter(Role.name == 'Горничная').first()
        r3 = db.session.query(Role.id).filter(Role.name == 'Администратор').first()
        r4 = db.session.query(Role.id).filter(Role.name == 'Старший администратор').first()
        r5 = db.session.query(Role.id).filter(Role.name == 'Инженер').first()
        maids = [
            Employee(email="admin@minihotel.local", username="admin", password="admin", last_name="super", first_name="admin", middle_name="adminovich", passport_data="none service", is_admin=True),
            Employee(email="user@minihotel.local", username="test_user", password="test", last_name="user", first_name="test", middle_name="tester", passport_data="test_user123456")
        ]
        if db.session.query(Employee.id).count() <= 25:
            for i in range(1, 12):
                p = gen_person()
                pf = p
                pm = p
                if i in range(1, 4):
                    while pf["gender"] == 'm':
                        pf = gen_person()
                    # add Служба горничных - Горничная
                    maids.append(Employee(role_id=r2[0],
                                          department_id=d1[0],
                                          email=(str(pf["login"]) + '@minihotel.local'),
                                          username=pf["login"],
                                          password='123456',
                                          last_name=pf["last"], first_name=pf["first"], middle_name=pf["midle"],
                                          passport_data=pf["passport"],
                                          date_of_employment=datetime(rand_date('yy'), rand_date('mm'), rand_date('dd'), 10,
                                                                      10,
                                                                      10).date()))
                elif i in range(4, 7):
                    while pm["gender"] != 'm':
                        pm = gen_person()
                    # add Служба приема и размещения - Администратор
                    maids.append(Employee(role_id=r3[0],
                                          department_id=d3[0],
                                          email=(str(pm["login"]) + '@minihotel.local'),
                                          username=pm["login"],
                                          password='123456',
                                          last_name=pm["last"], first_name=pm["first"], middle_name=pm["midle"],
                                          passport_data=pm["passport"],
                                          date_of_employment=datetime(rand_date('yy'), rand_date('mm'), rand_date('dd'), 10,
                                                                      10,
                                                                      10).date()))
                elif i in range(7, 9):
                    #add Инженер - Администратор гостиницы
                    maids.append(Employee(role_id=r5[0],
                                          department_id=d2[0],
                                          email=(str(p["login"]) + '@minihotel.local'),
                                          username=p["login"],
                                          password='123456',
                                          last_name=p["last"], first_name=p["first"], middle_name=p["midle"],
                                          passport_data=pf["passport"],
                                          date_of_employment=datetime(rand_date('yy'), rand_date('mm'), rand_date('dd'), 10,
                                                                      10,
                                                                      10).date()))

                elif i == 9:
                    while pf["gender"] == 'm':
                        pf = gen_person()
                    # add Служба горничных - Старшая горничная
                    maids.append(Employee(role_id=r1[0],
                                          department_id=d1[0],
                                          email=(str(pf["login"]) + '@minihotel.local'),
                                          username=pf["login"],
                                          password='123456',
                                          last_name=pf["last"], first_name=pf["first"], middle_name=pf["midle"],
                                          passport_data=pf["passport"],
                                          date_of_employment=datetime(rand_date('yy'), rand_date('mm'), rand_date('dd'), 10,
                                                                      10,
                                                                      10).date()))
                elif i in range(10, 12):
                    # add Администратор гостиницы - Старший администратор
                    maids.append(Employee(role_id=r4[0],
                                          department_id=d4[0],
                                          email=(str(p["login"]) + '@minihotel.local'),
                                          username=p["login"],
                                          password='123456',
                                          last_name=p["last"], first_name=p["first"], middle_name=p["midle"],
                                          passport_data=pf["passport"],
                                          date_of_employment=datetime(rand_date('yy'), rand_date('mm'), rand_date('dd'), 10,
                                                                      10,
                                                                      10).date()))
        for i in maids:
            if db.session.query(Employee.id).count() <= 25:
                if db.session.query(Employee.id).filter_by(email=i.email).scalar() is None:
                    if db.session.query(Employee.id).filter_by(passport_data=i.passport_data).scalar() is None:
                        db.session.add(i)
                        db.session.commit()
        if db.session.query(Rooms.id).count() <= 20:
            c1 = db.session.query(RoomsClass.id).filter(RoomsClass.class_name == 'Стандарт').first()
            c2 = db.session.query(RoomsClass.id).filter(RoomsClass.class_name == 'Комфорт').first()
            c3 = db.session.query(RoomsClass.id).filter(RoomsClass.class_name == 'Люкс').first()
            c4 = db.session.query(RoomsClass.id).filter(RoomsClass.class_name == 'Апартаменты').first()
            rooms = [
                Rooms(number=1,  bed=1, price=1000, class_id=c1[0]),
                Rooms(number=2,  bed=2, price=1300, class_id=c1[0]),
                Rooms(number=3,  bed=3, price=1600, class_id=c1[0]),
                Rooms(number=4,  bed=4, price=2000, class_id=c1[0]),
                Rooms(number=5,  bed=1, price=1500, class_id=c2[0]),
                Rooms(number=6,  bed=2, price=1800, class_id=c2[0]),
                Rooms(number=7,  bed=3, price=2200, class_id=c2[0]),
                Rooms(number=8,  bed=4, price=2500, class_id=c2[0]),
                Rooms(number=9,  bed=3, price=4000, class_id=c3[0]),
                Rooms(number=10,  bed=3, price=4000, class_id=c3[0]),
                Rooms(number=11,  bed=4, price=5000, class_id=c3[0]),
                Rooms(number=12,  bed=2, price=1800, class_id=c4[0]),
                Rooms(number=13,  bed=3, price=2400, class_id=c4[0]),
                Rooms(number=14,  bed=3, price=2400, class_id=c4[0]),
                Rooms(number=15,  bed=4, price=2800, class_id=c4[0]),
                Rooms(number=16,  bed=1, price=1000, class_id=c1[0]),
                Rooms(number=17,  bed=2, price=1300, class_id=c1[0]),
                Rooms(number=18,  bed=1, price=1000, class_id=c1[0]),
                Rooms(number=19,  bed=2, price=1800, class_id=c1[0]),
                Rooms(number=20,  bed=1, price=1500, class_id=c1[0])
                ]
            for i in rooms:
                if db.session.query(Rooms.id).filter_by(number=i.number).scalar() is None:
                    db.session.add(i)
                    db.session.commit()
        if db.session.query(Clients.id).count() <= 300:
            for i in range(1, 50):
                p = gen_person()
                print(p)
                clients=(Clients(email=(str(p["login"]) + '@minihotel.local'),
                                       username=p["login"], password='123456',
                                       last_name=p["last"], first_name=p["first"], middle_name=p["midle"],
                                       passport=p["passport"], registration=p["registration"],
                                       birthday=datetime(int(p["birthday"][0]), int(p["birthday"][1]), int(p["birthday"][2]), 10, 10, 10).date()))
                if db.session.query(Clients.id).count() <= 300:
                    if db.session.query(Clients.id).filter_by(email=clients.email).scalar() is None:
                        if db.session.query(Clients.id).filter_by(passport=clients.passport).scalar() is None:
                            if db.session.query(Clients.id).filter_by(username=clients.username).scalar() is None:
                                db.session.add(clients)
                                db.session.commit()

        if db.session.query(Orders.id).count() <= 300:
            for i in range(1, 150):
                cl_id = randint(1, 300)
                while db.session.query(Clients.id).filter_by(id=cl_id).scalar() is None:
                    cl_id = randint(1, 300)
                r_id = randint(1, 20)
                while db.session.query(Rooms.id).filter_by(id=r_id).scalar() is None:
                    r_id = randint(1, 20)
                date_arrival = datetime(rand_date('yy'),rand_date('mm'), rand_date('dd'), 10, 10, 10).date()
                date_leaving = date_arrival + timedelta(days=randint(1, 30))
                room_booked = True if randint(0,1) == 0 else False
                order_paid = False
                total_payment = int((date_leaving - date_arrival).days) * int(
                    db.session.query(Rooms.price).filter(Rooms.id == r_id).first()[0])
                if date_arrival < datetime.today().date():
                    # Дата заезда прошла => номер оплачен
                    order_paid = True
                elif date_arrival >  datetime.today().date():
                    # Дата заезда не настала => номер не оплачен
                    order_paid = False
                    room_booked = True
                if db.session.query(Orders.id).filter(Orders.date_of_leaving >= date_arrival, Orders.room_id == r_id).count() <= 0:
                    orders = (Orders(client_id=cl_id, room_id=r_id, date_of_arrival=date_arrival,
                                         date_of_leaving=date_leaving, room_booked=room_booked,
                                         order_paid=order_paid, total_payment=total_payment))

                    db.session.add(orders)
                    db.session.commit()


    @app.errorhandler(403)
    def forbidden(error):
        return render_template('errors/403.html', title='Forbidden'), 403

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('errors/404.html', title='Page Not Found'), 404

    @app.errorhandler(500)
    def internal_server_error(error):
        return render_template('errors/500.html', title='Server Error'), 500

    @app.route('/500')
    def error():
        abort(500)

    return app


